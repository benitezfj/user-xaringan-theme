---
title: "rmoo: An R Package for Multi-Objective Optimization"
author: "Francisco J. Benitez"
output:
  xaringan::moon_reader:
    css: [useR, useR-fonts]
    lib_dir: libs
---

class: chapter-slide

# Chapter

---

## Multi-Objective Optimization in R

Some text

### header


```r
# some code
seq(1:5)
```

```
## [1] 1 2 3 4 5
```

---

## Future Works and rmoo improvements

Some text

---

## External links and contact

Some text

---

## Acknowledgements

Some text

---

## Refences

Some text

---

## rmoo package

We will use the [viridis](https://cran.r-project.org/web/packages/viridis/vignettes/intro-to-viridis.html) package to create some colorblind-friendly plots. 
Let's see how the graph looks like with various color-vision-deficiency [simulations](https://github.com/clauswilke/colorblindr):




```r
fig <- ggplot(iris, aes(Sepal.Length, fill = Species)) + 
  geom_density(alpha = 0.7) + theme_useR() + 
  theme(legend.position = c(0.8, 0.9),legend.margin = margin(),
        legend.title = element_blank()) 
fig_grid <- cvd_grid(fig)
```

.center[
<img src="figure/unnamed-chunk-4-1.png" title="plot of chunk unnamed-chunk-4" alt="plot of chunk unnamed-chunk-4" width="40%" /><img src="figure/unnamed-chunk-4-2.png" title="plot of chunk unnamed-chunk-4" alt="plot of chunk unnamed-chunk-4" width="40%" />
]

---

Let's use a color scale that works better



```r
fig2 <- ggplot(iris, aes(Sepal.Length, fill = Species)) + 
  geom_density(alpha = 0.8) + theme_useR() + 
  theme(legend.position = c(0.8, 0.9))+ 
  scale_fill_viridis(discrete = TRUE)
fig_grid2 <- cvd_grid(fig2)
```


<img src="figure/unnamed-chunk-6-1.png" title="plot of chunk unnamed-chunk-6" alt="plot of chunk unnamed-chunk-6" width="48%" /><img src="figure/unnamed-chunk-6-2.png" title="plot of chunk unnamed-chunk-6" alt="plot of chunk unnamed-chunk-6" width="48%" />

---

## Do not forget to include alt-text to your figures!

Knitr (version >= 1.31) have a new feature to add alt-text to your figures. Just add fig.alt = "Your alt-text” in the chunk options.

<img src="figure/unnamed-chunk-7-1.png" title="Two ggplot panels, comparing default ggplot2 colors versus the ggplot2 result using viridis color palette. Each panel shows three superposed color-filled histograms. The default scale uses the colors salmon, light green and light blue, but viridis scale uses purple, aquamarine and yellow, which gives a better contrast." alt="Two ggplot panels, comparing default ggplot2 colors versus the ggplot2 result using viridis color palette. Each panel shows three superposed color-filled histograms. The default scale uses the colors salmon, light green and light blue, but viridis scale uses purple, aquamarine and yellow, which gives a better contrast." width="40%" /><img src="figure/unnamed-chunk-7-2.png" title="Two ggplot panels, comparing default ggplot2 colors versus the ggplot2 result using viridis color palette. Each panel shows three superposed color-filled histograms. The default scale uses the colors salmon, light green and light blue, but viridis scale uses purple, aquamarine and yellow, which gives a better contrast." alt="Two ggplot panels, comparing default ggplot2 colors versus the ggplot2 result using viridis color palette. Each panel shows three superposed color-filled histograms. The default scale uses the colors salmon, light green and light blue, but viridis scale uses purple, aquamarine and yellow, which gives a better contrast." width="40%" />


