<!DOCTYPE html>
<html lang="" xml:lang="">
  <head>
    <title>rmoo: An R Package for Multi-Objective Optimization</title>
    <meta charset="utf-8" />
    <meta name="author" content="Francisco J. Benitez" />
    <meta name="author" content="Diego P. Pinto-Roa" />
    <meta name="date" content="2022-06-02" />
    <script src="slides_files/header-attrs/header-attrs.js"></script>
    <link href="slides_files/remark-css/ninjutsu.css" rel="stylesheet" />
    <link rel="stylesheet" href="customthemer.css" type="text/css" />
  </head>
  <body>
    <textarea id="source">
class: center, middle, inverse, title-slide

.title[
# rmoo: An R Package for Multi-Objective Optimization
]
.author[
### Francisco J. Benitez<a href="#fn1" class="footnote-ref" id="fnref1"><sup>1</sup></a>
]
.author[
### Diego P. Pinto-Roa<a href="#fn2" class="footnote-ref" id="fnref2"><sup>2</sup></a>
]
.institute[
### Universidad Autonoma de Asuncion
]
.institute[
### Universidad Nacional de Asuncion
]
.date[
### June 02, 2022
]

---






&lt;style type="text/css"&gt;
.pull-left {
    float: left;
    width: 55%;
}

.pull-right {
    float: right;
    width: 45%;
}

.pull-right ~ p {
    clear: both;
}

&lt;/style&gt;


## Background

* __R__ has a large number of packages specialized in optimization, most of them are specific-purpose or single objective optimization.

* Inspired by other general-purpose optimization software in the reviewed literature and using the wide tools offered by __R__, we have developed the `rmoo` package.
&lt;figure&gt;
&lt;img src="optim.png"
     alt="List of multi-objective R packages" height = "270"
     style="float: left; margin-right: 10px;"
     &lt;figcaption&gt; List of multi-objective R packages available in Optimization section – CRAN Task View &lt;/figcaption&gt;
&lt;/figure&gt;

---

## `rmoo` Package  - Overview

* The __R__ Multi-Objective Optimization (`rmoo`) package provides the well-studied main family of Non-dominated Sorting Genetic Algorithms (NSGA).

* The `rmoo` package was proposed as a general-purpose framework for multi- and many-objective optimization, built as a fork of Luca Scrucca's GA package.

* It implements the multi-objective optimization (2 and 3 objectives) algorithms: NSGA-I and NSGA-II, and the many-objective optimization (more than 3 objectives) algorithm: NSGA-III

&lt;figure&gt;
&lt;img src="NSGA_evolution.png"
     alt="Algorithms rmoo" height = "195"
     style="float: left; margin-right: 10px;"
     &lt;figcaption&gt; Algorithms implemented by rmoo &lt;/figcaption&gt;
&lt;/figure&gt;

---

## `rmoo` Package - Installation

* User can install the stable version of rmoo in __R__ CRAN:


```r
devtools::install_github("Evolutionary-Optimization-Laboratory/rmoo")
```

* Alternatively, user can install the development version from Github:


```r
install.packages("rmoo")
```

&lt;figure&gt;
&lt;img src="rmoomessage.png"
     alt="rmoo message" height = "195"
     style="float: left; margin-right: 10px;"
&lt;/figure&gt;

---

## `rmoo` Package - Organization

.pull-left[
* `rmoo` allows the representation of results in binary, permutation or real-valued.

* Users will have a wide range of configuration options available during the optimization process.

* `rmoo` was developed to cover all steps in the optimization process: Problem definition, parameter configuration, plotting and performance metrics. Allowing reproducibility of results.
]

.pull-right[
&lt;figure&gt;
&lt;p&gt;&lt;img src="rmoo_architecture.png"
     alt="rmoo - Architecture" height = "400" width = "350" align = "center"
     style="float: center; margin-right: 10px;"
     &lt;figcaption align = "center" style="text-align: center"&gt; rmoo - Architecture &lt;/figcaption&gt;
&lt;/figure&gt;
]

---

## `rmoo` Package – Main Functions

.pull-left[
* `rmoo` is mainly made up of three functions that represent the implemented algorithms:
    * `nsga()` which contains the NSGA-I proposed by Srinivas &amp; Ded. This is a non-elitist multi-objective optimization algorithm.
    * `nsga2()` implements the NSGA-II proposed by Deb et al. It is
an NSGA-I improvement of its computational performance.
    * `nsga3()` contains the NSGA-III, which is a many-objective
reference point-based algorithm proposed by Deb &amp; Jain.
]

.pull-right[
&lt;figure&gt;
&lt;p&gt;&lt;img src="diagram_rmoo.png"
     alt="rmoo - Diagram" height = "390" align = "left"
     style="float: right; margin-right: 10px;"
     &lt;figcaption&gt; rmoo - Diagram &lt;/figcaption&gt;
&lt;/figure&gt;
]


---

## `rmoo` Package – Complementary Functions
.pull-left[
* In addition to the main functions, `rmoo` also includes plotting functions to facilitate their job to decision-makers:
    * `pcp()` the function plots a parallel coordinates. It is a 2-D graph in which all the result values are seen.
    * `heat_map()` plots a 2-D graph where objectives values are depicted as colors.
    * `polar()` represents each objective value of results based on the angle it covers.
    * `scatter()` It is one of the most used graphs to represent values. It represents the objectives values as points in a 2-D, 3-D or N-D plane. 
    * `monitor()` is the function called when the argument `monitor = TRUE`.
]

.pull-right[
&lt;figure&gt;
&lt;p&gt;&lt;img src="rmoo_plotting.png"
     alt="Plotting function dependencies" height = "390" align = "left"
     style="float: right; margin-right: 10px;"
     &lt;figcaption&gt; Plotting function dependencies &lt;/figcaption&gt;
&lt;/figure&gt;
]

---

## `rmoo` Package – Problem Definition

* The code snippet stating the DTLZ1 problem for 3 objective.


```r
dtlz1 &lt;- function(x, nobj = 3) {
    if(is.null(dim(x))) {
        x &lt;- matrix(x, 1)
    }
    n &lt;- ncol(x)
    y &lt;- matrix(x[,1:(nobj - 1)], nrow(x))
    z &lt;- matrix(x[, nobj:n], nrow(x))
    g &lt;- 100 * (n - nobj + 1 + rowSums((z - 0.5)^2 - cos(20 * pi * (z - 0.5))))
    tmp &lt;- t(apply(y, 1, cumprod))
    tmp &lt;- cbind(t(apply(tmp, 1, rev)) ,1)
    tmp2 &lt;- cbind(1, t(apply(1 - y, 1, rev)))
    f &lt;- tmp * tmp2 * 0.5 * (1 + g)
    return(f)
}
```


---

## `rmoo` Package – Problem Definition

* The `nsg3()` function call passing the necessary arguments for its use.



.pull-left[

```r
result &lt;- nsga3(type = "real-valued",
                fitness = dtlz1,
                lower = c(0,0,0),
                upper = c(1,1,1),
                popSize = 92,
                n_partitions = 12,
                monitor = TRUE,
                nObj = 3,
                maxiter = 500,
                seed = 45)
```
]

.pull-right[

```r
    #Head of the objective values
    head(result@fitness)
```

```
##              [,1]         [,2]         [,3]
## [1,] 4.168084e-01 8.322326e-02 2.132762e-06
## [2,] 1.666088e-01 4.178901e-02 2.916371e-01
## [3,] 3.333232e-01 3.541712e-05 1.666763e-01
## [4,] 8.946875e-05 8.335799e-02 4.165881e-01
## [5,] 1.668248e-01 1.666478e-01 1.665644e-01
## [6,] 4.674378e-04 2.915517e-01 2.080160e-01
```
]

---

## `rmoo` Package - Plotting

For the argument `monitor = TRUE` for any of the main functions, the `monitor()` function will be called during execution and it will plot the Pareto front generation by generation.

&lt;img src="myplot_1.png"
     alt="DTLZ1 static monitor" height = "300" align = "center"
     style="float: left; margin-right: 50px;"&gt;


&lt;img src="scatter_1.png"
     alt="Scatter static monitor" height = "300" align = "center"
     style="float: right; margin-right: 50px;"&gt;

---

## `rmoo` Package - Plotting

For the argument `monitor = TRUE` for any of the main functions, the `monitor()` function will be called during execution and it will plot the Pareto front generation by generation.

&lt;img src="DTLZ1.gif"
     alt="DTLZ1 monitor" height = "300" align = "center"
     style="float: left; margin-right: 50px;"&gt;


&lt;img src="scatter.gif"
     alt="Scatter monitor" height = "300" align = "center"
     style="float: right; margin-right: 50px;"&gt;

---

## `rmoo` Package – Scatter Plotting
* The `scatter()` function call generates the points plot, being able to make comparison with points supplied for 2-D and 3-D.

.pull-left[

```r
scatter(object = result)
```
]
.pull-right[

```r
scatter(object = result, 
        optimal = result@reference_points) 
```
]

&lt;div class = "row"&gt;
    &lt;div class = "column"&gt;
        &lt;img src="DTLZ1-scatter.png"
             alt="DTLZ1 scatter" height = "270" align = "left"
             style="float: left; margin-right: 50px;"&gt;
    &lt;/div&gt;
    &lt;div class = "column"&gt;
        &lt;img src="DTLZ1-scatter-reference.png"
        alt="DTLZ1 scatter with reference points" height = "270" 
        align = "right" style="float: right; margin-right: 50px;"&gt;
    &lt;/div&gt;
&lt;/div&gt;

---

## `rmoo` Package – Other Graphs

* The `pcp()` function plots a parallel coordinates. It is a 2-D graph in which all the result values are seen.


```r
pcp(object = result)
```

&lt;img src="DTLZ1-pcp.png"
             alt="DTLZ1 PCP" height = "270" 
             align = "center" style="float: center; margin-left: 150px;"&gt;
             
---

## `rmoo` Package – Other Graphs

* The `heat_map` function plots a 2-D graph where objectives values are depicted as colors.


```r
heat_map(object = result@fitness [1:5,])
```

&lt;img src="DTLZ2-heatmap.png"
        alt="DTLZ1 Heatmap" height = "270" 
        align = "center" style="float: right; margin-right: 50px;"&gt;
---

## `rmoo` Package – Other Graphs

* The `polar` function represents each objective value of results based on the angle it covers.


```r
polar(object = result@fitness [1:5,])
```

&lt;img src="DTLZ3-polar.png"
        alt="DTLZ1 Polar" height = "270" 
        align = "center" style="float: center; margin-right: 50px;"&gt;


---

##Future Works and Improvements for `rmoo`

* Implement other version of the Non-dominated Sorting Genetic Algorithms (R-NSGA-II, R-NSGA-III, U-NSGA-III), as well as Algorithms Based on Decomposition (MOEA/D).
* Modify the `rmoo` package to a component-oriented design, to give users more freedom over which components and modules to use. 
* rmoo no maneja restricciones, por lo que se implementara
---

## Our Contacts

.pull-left[
&lt;p&gt;&lt;img src="avatar_francisco.jpg"
             alt="Avatar Francisco" height = "250" 
             align = "left" style="float: left; margin-left: 70px;"&gt;
]

.pull-right[
&lt;p&gt;&lt;img src="DTLZ1-scatter-reference.png"
             alt="DTLZ1 scatter with reference points" height = "250" 
             align = "left" style="float: left; margin-left: 70px;"&gt;
]


.pull-left[
&lt;img src="github-brands.svg" height = "30"/&gt; https://www.github.com/benitezfj

&lt;img src="gitlab-brands.svg" height = "30"/&gt; https://www.gitlab.com/benitezfj 

&lt;img src="linkedin-brands.svg" height = "30"/&gt; https://www.linkedin.com/in/fjbenitez

&lt;img src="envelope-solid.svg" height = "30"/&gt; benitezfj94@gmail.com

&lt;img src="address-book-solid.svg" height = "30"/&gt; https://benitezfj.netfily.app

]

.pull-right[

&lt;img src="github-brands.svg" height = "30"/&gt; https://www.github.com/dppintoroa

&lt;img src="gitlab-brands.svg" height = "30"/&gt; https://www.gitlab.com/benitezfj 

&lt;img src="linkedin-brands.svg" height = "30"/&gt; https://www.linkedin.com/in/fjbenitez

&lt;img src="envelope-solid.svg" height = "30"/&gt; benitezfj94@gmail.com

&lt;img src="address-book-solid.svg" height = "30"/&gt; https://benitezfj.netfily.app


]

---

## References

- Theussl, S., Schwendinger, F., &amp; Borchers, H. W. (2022). CRAN Task View: Optimization and Mathematical Programming. Retrieved February 15, 2022, from https://cran.r-project.org/view=Optimization

- Srinivas N, Deb K (1994). “Muiltiobjective Optimization Using Nondominated Sorting in Genetic Algorithms.” Evolutionary Computation, 2(3), 221–248.

- Deb K, Pratap A, Agarwal S, Meyarivan T (2002). “A fast and elitist multiobjective genetic algorithm: NSGA-II.” IEEE Transactions on Evolutionary Computation, 6(2), 182–197

- Deb K, Jain H (2014). “An Evolutionary Many-Objective Optimization Algorithm Using Reference-Point-Based Nondominated Sorting Approach, Part I: Solving Problems With Box Constraints.” IEEE Transactions on Evolutionary Computation, 18(4), 577–601
    </textarea>
<style data-target="print-only">@media screen {.remark-slide-container{display:block;}.remark-slide-scaler{box-shadow:none;}}</style>
<script src="https://remarkjs.com/downloads/remark-latest.min.js"></script>
<script>var slideshow = remark.create();
if (window.HTMLWidgets) slideshow.on('afterShowSlide', function (slide) {
  window.dispatchEvent(new Event('resize'));
});
(function(d) {
  var s = d.createElement("style"), r = d.querySelector(".remark-slide-scaler");
  if (!r) return;
  s.type = "text/css"; s.innerHTML = "@page {size: " + r.style.width + " " + r.style.height +"; }";
  d.head.appendChild(s);
})(document);

(function(d) {
  var el = d.getElementsByClassName("remark-slides-area");
  if (!el) return;
  var slide, slides = slideshow.getSlides(), els = el[0].children;
  for (var i = 1; i < slides.length; i++) {
    slide = slides[i];
    if (slide.properties.continued === "true" || slide.properties.count === "false") {
      els[i - 1].className += ' has-continuation';
    }
  }
  var s = d.createElement("style");
  s.type = "text/css"; s.innerHTML = "@media print { .has-continuation { display: none; } }";
  d.head.appendChild(s);
})(document);
// delete the temporary CSS (for displaying all slides initially) when the user
// starts to view slides
(function() {
  var deleted = false;
  slideshow.on('beforeShowSlide', function(slide) {
    if (deleted) return;
    var sheets = document.styleSheets, node;
    for (var i = 0; i < sheets.length; i++) {
      node = sheets[i].ownerNode;
      if (node.dataset["target"] !== "print-only") continue;
      node.parentNode.removeChild(node);
    }
    deleted = true;
  });
})();
// add `data-at-shortcutkeys` attribute to <body> to resolve conflicts with JAWS
// screen reader (see PR #262)
(function(d) {
  let res = {};
  d.querySelectorAll('.remark-help-content table tr').forEach(tr => {
    const t = tr.querySelector('td:nth-child(2)').innerText;
    tr.querySelectorAll('td:first-child .key').forEach(key => {
      const k = key.innerText;
      if (/^[a-z]$/.test(k)) res[k] = t;  // must be a single letter (key)
    });
  });
  d.body.setAttribute('data-at-shortcutkeys', JSON.stringify(res));
})(document);
(function() {
  "use strict"
  // Replace <script> tags in slides area to make them executable
  var scripts = document.querySelectorAll(
    '.remark-slides-area .remark-slide-container script'
  );
  if (!scripts.length) return;
  for (var i = 0; i < scripts.length; i++) {
    var s = document.createElement('script');
    var code = document.createTextNode(scripts[i].textContent);
    s.appendChild(code);
    var scriptAttrs = scripts[i].attributes;
    for (var j = 0; j < scriptAttrs.length; j++) {
      s.setAttribute(scriptAttrs[j].name, scriptAttrs[j].value);
    }
    scripts[i].parentElement.replaceChild(s, scripts[i]);
  }
})();
(function() {
  var links = document.getElementsByTagName('a');
  for (var i = 0; i < links.length; i++) {
    if (/^(https?:)?\/\//.test(links[i].getAttribute('href'))) {
      links[i].target = '_blank';
    }
  }
})();</script>

<script>
slideshow._releaseMath = function(el) {
  var i, text, code, codes = el.getElementsByTagName('code');
  for (i = 0; i < codes.length;) {
    code = codes[i];
    if (code.parentNode.tagName !== 'PRE' && code.childElementCount === 0) {
      text = code.textContent;
      if (/^\\\((.|\s)+\\\)$/.test(text) || /^\\\[(.|\s)+\\\]$/.test(text) ||
          /^\$\$(.|\s)+\$\$$/.test(text) ||
          /^\\begin\{([^}]+)\}(.|\s)+\\end\{[^}]+\}$/.test(text)) {
        code.outerHTML = code.innerHTML;  // remove <code></code>
        continue;
      }
    }
    i++;
  }
};
slideshow._releaseMath(document);
</script>
<!-- dynamically load mathjax for compatibility with self-contained -->
<script>
(function () {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src  = 'https://mathjax.rstudio.com/latest/MathJax.js?config=TeX-MML-AM_CHTML';
  if (location.protocol !== 'file:' && /^https?:/.test(script.src))
    script.src  = script.src.replace(/^https?:/, '');
  document.getElementsByTagName('head')[0].appendChild(script);
})();
</script>
  </body>
</html>
