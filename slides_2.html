<!DOCTYPE html>
<html lang="" xml:lang="">
  <head>
    <title>rmoo: An R Package for Multi-Objective Optimization</title>
    <meta charset="utf-8" />
    <meta name="author" content="Francisco J. Benitez" />
    <meta name="author" content="Diego P. Pinto-Roa" />
    <meta name="date" content="2022-06-02" />
    <script src="slides_files/header-attrs-2.11/header-attrs.js"></script>
    <link rel="stylesheet" href="customthemer.css" type="text/css" />
  </head>
  <body>
    <textarea id="source">
class: center, middle, inverse, title-slide

# rmoo: An R Package for Multi-Objective Optimization
### Francisco J. Benitez<sup>1</sup> &emsp; Diego P. Pinto-Roa<sup>2</sup>

#### <sup>1</sup>Facultad de Ciencias y Tecnologias
#### Universidad Autonoma de Asuncion

#### <sup>2</sup>Facultad Politecnica
#### Universidad Nacional de Asuncion
### June 02, 2022

---

&lt;style type="text/css"&gt;
.pull-left {
    float: left;
    width: 44%;
}

.pull-right {
    float: right;
    width: 44%;
}

.pull-right ~ p {
    clear: both;
}

&lt;/style&gt;

## Background

* __R__ has a large number of packages specialized in optimization, most of them are specific-purpose or single objective optimization.

* Inspired by other general-purpose optimization software in the reviewed literature and using the wide tools offered by __R__, we have developed the `rmoo` package.
&lt;figure&gt;
&lt;img src="optim.png"
     alt="List of multi-objective R packages" height = "270"
     style="float: left; margin-right: 10px;"
     &lt;figcaption&gt; List of multi-objective R packages available in Optimization section – CRAN Task View &lt;/figcaption&gt;
&lt;/figure&gt;

---

##The R Package rmoo - Overview

* The __R__ Multi-Objective Optimization (_rmoo_) package provides the well-studied main family of Non-dominated Sorting Genetic Algorithms (NSGA).

* The `rmoo` package was proposed as a general-purpose framework for multi- and many-objective optimization, built as a fork of Luca Scrucca's GA package.

* It implements the multi-objective optimization (2 and 3 objectives) algorithms: NSGA-I and NSGA-II, and the many-objective optimization (more than 3 objectives) algorithm: NSGA-III

&lt;figure&gt;
&lt;img src="NSGA_evolution.png"
     alt="Algorithms rmoo" height = "195"
     style="float: left; margin-right: 10px;"
     &lt;figcaption&gt; Algorithms implemented by rmoo &lt;/figcaption&gt;
&lt;/figure&gt;

---

##The R Package rmoo - Installation

* User can install the stable version of rmoo in __R__ CRAN:


```r
devtools::install_github("Evolutionary-Optimization-Laboratory/rmoo")
```

* Alternatively, user can install the development version from Github:


```r
install.packages("rmoo")
```

&lt;figure&gt;
&lt;img src="rmoomessage.png"
     alt="rmoo message" height = "195"
     style="float: left; margin-right: 10px;"
&lt;/figure&gt;

---

##The R Package rmoo - Organization

&lt;figure&gt;
&lt;img src="rmoo_architecture.png"
     alt="rmoo - Architecture" height = "390" align = "left"
     style="float: right; margin-right: 10px;"
     &lt;figcaption&gt; rmoo - Architecture &lt;/figcaption&gt;
&lt;/figure&gt;

* `rmoo` allows the representation of results in binary, permutation or real-valued.

* Users will have a wide range of configuration options available during the optimization process.

* `rmoo` was developed to cover all steps in the optimization process: Problem definition, parameter configuration, plotting and performance metrics. Allowing reproducibility of results.

---

## The R Package rmoo – Main Functions

&lt;figure&gt;
&lt;img src="diagram_rmoo.png"
     alt="rmoo - Diagram" height = "390" align = "left"
     style="float: right; margin-right: 10px;"
     &lt;figcaption&gt; rmoo - Diagram &lt;/figcaption&gt;
&lt;/figure&gt;

* `rmoo` is mainly made up of three functions that represent the implemented algorithms:
    * `nsga()` which contains the NSGA-I proposed by Srinivas &amp; Ded. This is a non-elitst multi-objective optimization algorithm.
    * `nsga2()` simplements the NSGA-II proposed by Deb et al. It is
an NSGA-I improvement of its computational performance.
    * `nsga3()` scontains the NSGA-III, which is a many-objective
reference point-based algorithm proposed by Deb &amp; Jain.

---

## The R Package rmoo – Complementary Functions

&lt;figure&gt;
&lt;img src="rmoo_plotting.png"
     alt="Plotting function dependencies" height = "390" align = "left"
     style="float: right; margin-right: 10px;"
     &lt;figcaption&gt; Plotting function dependencies &lt;/figcaption&gt;
&lt;/figure&gt;

* In addition to the main functions, `rmoo` also includes plotting functions to facilitate their job to decision-makers:
    * `pcp()` the function plots a parallel coordinates. It is a 2-D graph in which all the result values are seen.
    * `heat_map()` If you want, you can insert your multimedia content here.
    * `polar()` If you want, you can insert your multimedia content.
    * `scatter()` If you want, you can insert your multimedia content.
    * `monitor()` sIf you want, you can insert your multimedia
content

---

## rmoo Package – Problem Definition

* The code snippet stating the DTLZ1 problem for 3 objective.


```r
dtlz1 &lt;- function(x, nobj = 3) {
    if(is.null(dim(x))) {
        x &lt;- matrix(x, 1)
    }
    n &lt;- ncol(x)
    y &lt;- matrix(x[,1:(nobj - 1)], nrow(x))
    z &lt;- matrix(x[, nobj:n], nrow(x))
    g &lt;- 100 * (n - nobj + 1 + rowSums((z - 0.5)^2 - cos(20 * pi * (z - 0.5))))
    tmp &lt;- t(apply(y, 1, cumprod))
    tmp &lt;- cbind(t(apply(tmp, 1, rev)) ,1)
    tmp2 &lt;- cbind(1, t(apply(1 - y, 1, rev)))
    f &lt;- tmp * tmp2 * 0.5 * (1 + g)
    return(f)
}
```


---

## rmoo Package – Problem Definition

* The `nsg3()` function call passing the necessary arguments for its use.



.pull-left[

```r
result &lt;- nsga3(type = "real-valued",
                fitness = dtlz1,
                lower = c(0,0,0),
                upper = c(1,1,1),
                popSize = 92,
                n_partitions = 12,
                monitor = TRUE,
                nObj = 3,
                maxiter = 500,
                seed = 45)
```
]

.pull-right[

```r
    #Head of the objective values
    head(result@fitness)
```

```
##              [,1]         [,2]         [,3]
## [1,] 4.168084e-01 8.322326e-02 2.132762e-06
## [2,] 1.666088e-01 4.178901e-02 2.916371e-01
## [3,] 3.333232e-01 3.541712e-05 1.666763e-01
## [4,] 8.946875e-05 8.335799e-02 4.165881e-01
## [5,] 1.668248e-01 1.666478e-01 1.665644e-01
## [6,] 4.674378e-04 2.915517e-01 2.080160e-01
```
]

---

## The R Package rmoo - Plotting

For the argument `monitor=TRUE` for any of the main functions. It will generate a plot while execution is performed for a two-objective function.

&lt;img src="myplot_1.png"
     alt="DTLZ1 static monitor" height = "300" align = "center"
     style="float: left; margin-right: 50px;"&gt;


&lt;img src="scatter_1.png"
     alt="Scatter static monitor" height = "300" align = "center"
     style="float: right; margin-right: 50px;"&gt;

---

## The R Package rmoo - Plotting

For the argument `monitor=TRUE` for any of the main functions. It will generate a plot while execution is performed for a two-objective function.

&lt;img src="DTLZ1.gif"
     alt="DTLZ1 monitor" height = "300" align = "center"
     style="float: left; margin-right: 50px;"&gt;


&lt;img src="scatter.gif"
     alt="Scatter monitor" height = "300" align = "center"
     style="float: right; margin-right: 50px;"&gt;

---

##The R Package rmoo – Scatter Plotting
* This function call will generate the right plot, function call will generate the left plot

.pull-left[

```r
scatter(object = result)
```
]
.pull-right[

```r
scatter(object = result, 
        optimal = result@reference_points) 
```
]

&lt;div class = "row"&gt;
    &lt;div class = "column"&gt;
        &lt;img src="DTLZ1-scatter.png"
             alt="DTLZ1 scatter" height = "270" align = "left"
             style="float: left; margin-right: 50px;"&gt;
    &lt;/div&gt;
    &lt;div class = "column"&gt;
        &lt;img src="DTLZ1-scatter-reference.png"
        alt="DTLZ1 scatter with reference points" height = "270" 
        align = "right" style="float: right; margin-right: 50px;"&gt;
    &lt;/div&gt;
&lt;/div&gt;

---

##The R Package `rmoo` – Other Graphs

.pull-left[

```r
pcp(object = result)
```
]
.pull-right[

```r
heat_map(object = result@fitness [1:5,])
```
]

&lt;div class = "row"&gt;
    &lt;div class = "column"&gt;
        &lt;img src="DTLZ1-pcp.png"
             alt="DTLZ1 PCP" height = "270" align = "left"
             style="float: left; margin-right: 50px;"&gt;
    &lt;/div&gt;
    &lt;div class = "column"&gt;
        &lt;img src="DTLZ2-heatmap.png"
        alt="DTLZ1 Heatmap" height = "270" 
        align = "right" style="float: right; margin-right: 50px;"&gt;
    &lt;/div&gt;
&lt;/div&gt;
---

##The R Package `rmoo` – Other Graphs


```r
polar(object = result@fitness [1:5,])
```

&lt;img src="DTLZ3-polar.png"
        alt="DTLZ1 Polar" height = "270" 
        align = "right" style="float: right; margin-right: 50px;"&gt;


---

##Future Works and Improvements for `rmoo`

This is an R Markdown presentation. Markdown is a simple formatting syntax for authoring HTML, PDF, and MS Word documents. For more details on using R Markdown see &lt;http://rmarkdown.rstudio.com&gt;.

When you click the **Knit** button a document will be generated that includes both content as well as the output of any embedded R code chunks within the document.

- Bullet 1
- Bullet 2
- Bullet 3

---

## Our Contacts


```r
summary(cars)
```

```
##      speed           dist       
##  Min.   : 4.0   Min.   :  2.00  
##  1st Qu.:12.0   1st Qu.: 26.00  
##  Median :15.0   Median : 36.00  
##  Mean   :15.4   Mean   : 42.98  
##  3rd Qu.:19.0   3rd Qu.: 56.00  
##  Max.   :25.0   Max.   :120.00
```

---

## References


```r
library(rmoo)
```

---

## Acknowledgements

![](slides_files/figure-html/pressure-1.png)&lt;!-- --&gt;

    </textarea>
<style data-target="print-only">@media screen {.remark-slide-container{display:block;}.remark-slide-scaler{box-shadow:none;}}</style>
<script src="https://remarkjs.com/downloads/remark-latest.min.js"></script>
<script>var slideshow = remark.create();
if (window.HTMLWidgets) slideshow.on('afterShowSlide', function (slide) {
  window.dispatchEvent(new Event('resize'));
});
(function(d) {
  var s = d.createElement("style"), r = d.querySelector(".remark-slide-scaler");
  if (!r) return;
  s.type = "text/css"; s.innerHTML = "@page {size: " + r.style.width + " " + r.style.height +"; }";
  d.head.appendChild(s);
})(document);

(function(d) {
  var el = d.getElementsByClassName("remark-slides-area");
  if (!el) return;
  var slide, slides = slideshow.getSlides(), els = el[0].children;
  for (var i = 1; i < slides.length; i++) {
    slide = slides[i];
    if (slide.properties.continued === "true" || slide.properties.count === "false") {
      els[i - 1].className += ' has-continuation';
    }
  }
  var s = d.createElement("style");
  s.type = "text/css"; s.innerHTML = "@media print { .has-continuation { display: none; } }";
  d.head.appendChild(s);
})(document);
// delete the temporary CSS (for displaying all slides initially) when the user
// starts to view slides
(function() {
  var deleted = false;
  slideshow.on('beforeShowSlide', function(slide) {
    if (deleted) return;
    var sheets = document.styleSheets, node;
    for (var i = 0; i < sheets.length; i++) {
      node = sheets[i].ownerNode;
      if (node.dataset["target"] !== "print-only") continue;
      node.parentNode.removeChild(node);
    }
    deleted = true;
  });
})();
(function() {
  "use strict"
  // Replace <script> tags in slides area to make them executable
  var scripts = document.querySelectorAll(
    '.remark-slides-area .remark-slide-container script'
  );
  if (!scripts.length) return;
  for (var i = 0; i < scripts.length; i++) {
    var s = document.createElement('script');
    var code = document.createTextNode(scripts[i].textContent);
    s.appendChild(code);
    var scriptAttrs = scripts[i].attributes;
    for (var j = 0; j < scriptAttrs.length; j++) {
      s.setAttribute(scriptAttrs[j].name, scriptAttrs[j].value);
    }
    scripts[i].parentElement.replaceChild(s, scripts[i]);
  }
})();
(function() {
  var links = document.getElementsByTagName('a');
  for (var i = 0; i < links.length; i++) {
    if (/^(https?:)?\/\//.test(links[i].getAttribute('href'))) {
      links[i].target = '_blank';
    }
  }
})();</script>

<script>
slideshow._releaseMath = function(el) {
  var i, text, code, codes = el.getElementsByTagName('code');
  for (i = 0; i < codes.length;) {
    code = codes[i];
    if (code.parentNode.tagName !== 'PRE' && code.childElementCount === 0) {
      text = code.textContent;
      if (/^\\\((.|\s)+\\\)$/.test(text) || /^\\\[(.|\s)+\\\]$/.test(text) ||
          /^\$\$(.|\s)+\$\$$/.test(text) ||
          /^\\begin\{([^}]+)\}(.|\s)+\\end\{[^}]+\}$/.test(text)) {
        code.outerHTML = code.innerHTML;  // remove <code></code>
        continue;
      }
    }
    i++;
  }
};
slideshow._releaseMath(document);
</script>
<!-- dynamically load mathjax for compatibility with self-contained -->
<script>
(function () {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src  = 'https://mathjax.rstudio.com/latest/MathJax.js?config=TeX-MML-AM_CHTML';
  if (location.protocol !== 'file:' && /^https?:/.test(script.src))
    script.src  = script.src.replace(/^https?:/, '');
  document.getElementsByTagName('head')[0].appendChild(script);
})();
</script>
  </body>
</html>
